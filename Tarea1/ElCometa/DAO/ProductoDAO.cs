﻿using ElCometa.Entities;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElCometa.DAO
{
    class ProductoDAO
    {

        static NpgsqlConnection conexion;
        static NpgsqlCommand cmd;

        public void InsertarDatos(Producto pr)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            cmd = new NpgsqlCommand("INSERT INTO productos (clave, costMatPrim, costProduc, precVent) VALUES ('" + pr.clave + "', '" + pr.costMatPrim + "', '" + pr.costProduc + "', '" + pr.precVent + "')", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        public void ModificarDatos(Producto pr)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("UPDATE productos SET costMatPrim = '" + pr.costMatPrim + "', costProduc = '" + pr.costProduc + "', precVent = '" + pr.precVent + "' WHERE clave = '" + pr.clave + "'", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        public void EliminarDatos(string clave)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("DELETE FROM productos WHERE clave = '" + clave + "'", conexion);
            cmd.ExecuteNonQuery();
            conexion.Close();
        }

        public Producto ConsultarDatos(string clave)
        {
            conexion = Conexion.ConexionS();
            conexion.Open();
            NpgsqlCommand cmd = new NpgsqlCommand("SELECT clave, costMatPrim, costProduc, precVent FROM productos WHERE clave = '" + clave + "'", conexion);
            NpgsqlDataReader dr = cmd.ExecuteReader();
            if (dr.HasRows)
            {
                if (dr.Read())
                {
                    Producto pr = new Producto();
                    pr.clave = dr.GetString(0);
                    pr.costMatPrim = dr.GetDouble(1);
                    pr.costProduc = dr.GetDouble(2);
                    pr.precVent = dr.GetDouble(3);
                    return pr;
                }

            }
            conexion.Close();

            return null;
        }

    }
}
