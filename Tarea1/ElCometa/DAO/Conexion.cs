﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElCometa.DAO
{
    class Conexion
    {
        static NpgsqlConnection conexion;

        public static NpgsqlConnection ConexionS()
        {
            string servidor = "localhost";
            int puerto = 5432;
            string usuario = "postgres";
            string clave = "postgres";
            string baseDatos = "ElCometa";

            string cadenaConexion = "Server=" + servidor + ";" + "Port=" + puerto + ";" + "User Id=" + usuario + ";" + "Password=" + clave + ";" + "Database=" + baseDatos;
            return conexion = new NpgsqlConnection(cadenaConexion);
        }
    }
}
