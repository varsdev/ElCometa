﻿using ElCometa.DAO;
using ElCometa.Entities;
using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElCometa.BO
{
    class ProductoBO
    {
        public double matPrim;

        public double manObr;

        public double gastFrab;

        public double produc;

        public double prec;

        Producto pr = new Producto();
        ProductoDAO pdao = new ProductoDAO();

        public double costProduc(string clave, double costMatPrim, bool edit)
        {
            matPrim = costMatPrim;

            if (clave == "1")
            {
                manObr = matPrim + (matPrim * 0.80);

                gastFrab = matPrim + (matPrim * 0.28);

                produc = manObr + gastFrab;

                prec = produc + (produc * 0.45);

                pr.clave = clave;
                pr.costMatPrim = Math.Round(costMatPrim);
                pr.costProduc = Math.Round(produc);
                pr.precVent = Math.Round(prec);

                if (edit)
                {
                    pdao.ModificarDatos(pr);
                }
                else
                {
                    pdao.InsertarDatos(pr);
                }

            }
            else if (clave == "2")
            {
                manObr = matPrim + (matPrim * 0.85);

                gastFrab = matPrim + (matPrim * 0.30);

                produc = manObr + gastFrab;

                prec = produc + (produc * 0.45);

                pr.clave = clave;
                pr.costMatPrim = Math.Round(costMatPrim);
                pr.costProduc = Math.Round(produc);
                pr.precVent = Math.Round(prec);

                if (edit)
                {
                    pdao.ModificarDatos(pr);
                }
                else
                {
                    pdao.InsertarDatos(pr);
                }

            }
            else if (clave == "3")
            {
                manObr = matPrim + (matPrim * 0.75);

                gastFrab = matPrim + (matPrim * 0.35);

                produc = manObr + gastFrab;

                prec = produc + (produc * 0.45);

                pr.clave = clave;
                pr.costMatPrim = Math.Round(costMatPrim);
                pr.costProduc = Math.Round(produc);
                pr.precVent = Math.Round(prec);

                if (edit)
                {
                    pdao.ModificarDatos(pr);
                }
                else
                {
                    pdao.InsertarDatos(pr);
                }

            }
            else if (clave == "4")
            {
                manObr = matPrim + (matPrim * 0.75);

                gastFrab = matPrim + (matPrim * 0.28);

                produc = manObr + gastFrab;

                prec = produc + (produc * 0.45);

                pr.clave = clave;
                pr.costMatPrim = Math.Round(costMatPrim);
                pr.costProduc = Math.Round(produc);
                pr.precVent = Math.Round(prec);

                if (edit)
                {
                    pdao.ModificarDatos(pr);
                }
                else
                {
                    pdao.InsertarDatos(pr);
                }

            }
            else if (clave == "5")
            {
                manObr = matPrim + (matPrim * 0.80);

                gastFrab = matPrim + (matPrim * 0.30);

                produc = manObr + gastFrab;

                prec = produc + (produc * 0.45);

                pr.clave = clave;
                pr.costMatPrim = Math.Round(costMatPrim);
                pr.costProduc = Math.Round(produc);
                pr.precVent = Math.Round(prec);

                if (edit)
                {
                    pdao.ModificarDatos(pr);
                }
                else
                {
                    pdao.InsertarDatos(pr);
                }

            }
            else if (clave == "6")
            {
                manObr = matPrim + (matPrim * 0.85);

                gastFrab = matPrim + (matPrim * 0.35);

                produc = manObr + gastFrab;

                prec = produc + (produc * 0.45);

                pr.clave = clave;
                pr.costMatPrim = Math.Round(costMatPrim);
                pr.costProduc = Math.Round(produc);
                pr.precVent = Math.Round(prec);

                if (edit)
                {
                    pdao.ModificarDatos(pr);
                }
                else
                {
                    pdao.InsertarDatos(pr);
                }

            }

            return Math.Round(produc);
        }

        public double precVent(double produc)
        {
            prec = produc + (produc * 0.45);

            return Math.Round(prec);
        }

    }
}
