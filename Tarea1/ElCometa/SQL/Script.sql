CREATE TABLE productos
(
	id serial primary key,
	clave text not null,
	costMatPrim numeric not null,
	costProduc numeric not null,
	precVent numeric not null,
	CONSTRAINT unq_productos_clave UNIQUE (clave)
);