﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElCometa.Entities
{
    class Producto
    {
        public int id { get; set; }

        public string clave { get; set; }

        public double costMatPrim { get; set; }

        public double costProduc { get; set; }

        public double precVent { get; set; }


        public string impresion()
        {
            return "Clave: " + clave + " Materia Prima: " + costMatPrim + " Costo Producccion: " + costProduc + " Precio venta: " + precVent;
        }

    }
}
