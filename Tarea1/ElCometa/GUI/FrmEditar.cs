﻿using ElCometa.BO;
using ElCometa.DAO;
using ElCometa.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElCometa.GUI
{
    public partial class FrmEditar : Form
    {
        public FrmEditar()
        {
            InitializeComponent();
            this.CenterToScreen();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            string clave = txtClave.Text.Trim();
            ProductoDAO pdao = new ProductoDAO();
            if (pdao.ConsultarDatos(clave) != null)
            {
                Producto pr = pdao.ConsultarDatos(clave);
                txtCostMatPrim.Text = pr.costMatPrim.ToString();
                txtProduc.Text = pr.costProduc.ToString();
                txtVent.Text = pr.precVent.ToString();               
            }
            else
            {
                txtClave.ResetText();
                txtCostMatPrim.ResetText();
                txtProduc.ResetText();
                txtVent.ResetText();
                MessageBox.Show("Clave no registrada");
            }


        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            ProductoBO pbo = new ProductoBO();

            double matPrim = double.Parse(txtCostMatPrim.Text.Trim());
            string clave = txtClave.Text.Trim();
            double costProduc = pbo.costProduc(clave, matPrim, true);
            double precVent = pbo.precVent(costProduc);
            txtProduc.Text = costProduc.ToString();
            txtVent.Text = precVent.ToString();
            MessageBox.Show("Clave Editada");

        }

        private void FrmEditar_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form frm = new FrmMenu();
            frm.Show();
        }
    }
}