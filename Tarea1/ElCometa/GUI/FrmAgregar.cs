﻿using ElCometa.BO;
using ElCometa.GUI;
using Npgsql;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElCometa
{
    public partial class FrmRegistrar : Form
    {
        public FrmRegistrar()
        {
            InitializeComponent();
            this.CenterToScreen();
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {          
            ProductoBO pbo = new ProductoBO();

            double matPrim = double.Parse(txtCostMatPrim.Text.Trim());
            string clave = txtClave.Text.Trim();
            double costProduc = pbo.costProduc(clave, matPrim, false);
            double precVent = pbo.precVent(costProduc);
            txtProduc.Text = costProduc.ToString();
            txtVent.Text = precVent.ToString();
            btnCalcular.Enabled = false;
            MessageBox.Show("Clave Registrada");
        }

        private void FrmRegistrar_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form frm = new FrmMenu();
            frm.Show();
        }

        private void FrmRegistrar_Load(object sender, EventArgs e)
        {

        }
    }
}
