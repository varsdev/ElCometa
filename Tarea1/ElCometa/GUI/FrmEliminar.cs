﻿using ElCometa.DAO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ElCometa.GUI
{
    public partial class FrmEliminar : Form
    {
        public FrmEliminar()
        {
            InitializeComponent();
            this.CenterToScreen();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            string clave = txtClave.Text.Trim();
            ProductoDAO pdao = new ProductoDAO();
            pdao.EliminarDatos(clave);
            txtClave.ResetText();
            MessageBox.Show("Clave eliminada");
        }

        private void FrmEliminar_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form frm = new FrmMenu();
            frm.Show();
        }
    }
}
